'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/aboutUs/getData', controller.aboutUs.getData);
  router.get('/slideshow/getSlideshow', controller.slideshow.getSlideshow);
  router.get('/attribute/getDataById', controller.attribute.getDataById);
  router.post('/askList/add', controller.askList.add);
  router.get('/product/getProducts', controller.product.getProducts);
  router.get('/news/getNews', controller.news.getNews);
  router.get('/solutions/getSolutions', controller.solution.getSolutions);
  router.get('/successfulCase/getSuccessfulCase', controller.successfulCase.getSuccessfulCase);
};
