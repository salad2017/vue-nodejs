'use strict';

const Service = require('egg').Service;
const util = require('../../utils/util.js');

class askListervice extends Service {
  async add(askList) {
    const data = await this.app.mysql.insert('ask_list', { id: util.randomString(32), name: askList.name, email: askList.email, phone: askList.phone, compony: askList.compony, title: askList.title, content: askList.content, product_id: askList.productId, create_time: new Date(), create_user: askList.phone, status: 0 });
    return data;
  }
}

module.exports = askListervice;
