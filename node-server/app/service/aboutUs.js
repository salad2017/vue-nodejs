// app/service/aboutUs.js
'use strict';

const Service = require('egg').Service;

class AboutUsService extends Service {
  async getData() {
    const data = await this.app.mysql.query('SELECT * FROM `about_us`');
    return data;
  }
}

module.exports = AboutUsService;
