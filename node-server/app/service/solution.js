// app/service/aboutUs.js
'use strict';

const Service = require('egg').Service;

class SolutionService extends Service {
  async getSolutions(obj) {
    const data = await this.app.mysql.select('solution', {
      limit: obj.pageSize - 0,
      offset: (obj.current - 1) * (obj.pageSize - 0),
    });
    return data;
  }
}

module.exports = SolutionService;
