// app/service/news.js
'use strict';

const Service = require('egg').Service;

class NewsService extends Service {
  async getNews(obj) {
    const data = await this.app.mysql.select('news', {
      limit: obj.pageSize - 0,
      offset: (obj.current - 1) * (obj.pageSize - 0),
    });
    return data;
  }
  async getToatl() {
    const data = await this.app.mysql.query('select count(*) from news');
    return data[0]['count(*)'];
  }
}

module.exports = NewsService;
