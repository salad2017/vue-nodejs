// app/service/aboutUs.js
'use strict';

const Service = require('egg').Service;

class AttributeService extends Service {
  async getDataById(id, pageSize, current) {

    let opts = { where: { attrId: id } };
    if (pageSize && current) {
      opts = Object.assign(opts, {
        limit: pageSize - 0,
        offset: (current - 1) * (pageSize - 0),
      });
    }
    const data = await this.app.mysql.select('sys_attribute_detail', opts);
    return data;
  }
}

module.exports = AttributeService;
