// app/service/aboutUs.js
'use strict';

const Service = require('egg').Service;

class ProductService extends Service {
  async getProducts(obj) {

    const opts = {
      limit: obj.pageSize - 0,
      offset: (obj.current - 1) * (obj.pageSize - 0),
    };
    if (obj.typeId) {
      opts.where = {
        type_id: obj.typeId,
      };
    }
    const data = await this.app.mysql.select('product', opts);
    return data;
  }
  async getToatl(obj) {
    let sql = 'select count(*) from product';
    if (obj.typeId) {
      sql = sql + ' where type_id = "' + obj.typeId + '"';
    }
    const data = await this.app.mysql.query(sql);
    return data[0]['count(*)'];
  }
}

module.exports = ProductService;
