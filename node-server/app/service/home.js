'use strict';

const Service = require('egg').Service;

class HomeService extends Service {
  async getSlideshow() {
    const data = await this.app.mysql.select('slideshow', {
      limit: 2,
    });
    return data;
  }
}

module.exports = HomeService;
