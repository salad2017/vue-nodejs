// app/service/aboutUs.js
'use strict';

const Service = require('egg').Service;

class SuccessfulCaseService extends Service {
  async getSuccessfulCase(obj) {
    const data = await this.app.mysql.select('successful_case', {
      limit: obj.pageSize - 0,
      offset: (obj.current - 1) * (obj.pageSize - 0),
    });
    return data;
  }
}

module.exports = SuccessfulCaseService;
