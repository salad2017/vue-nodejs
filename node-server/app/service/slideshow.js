// app/service/aboutUs.js
'use strict';

const Service = require('egg').Service;

class SlideshowService extends Service {
  async getSlideshow(obj) {
    const data = await this.app.mysql.select('slideshow', {
      limit: obj.pageSize - 0,
      offset: (obj.current - 1) * (obj.pageSize - 0),
    });
    return data;
  }
}

module.exports = SlideshowService;
