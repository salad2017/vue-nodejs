'use strict';

const BaseController = require('./base.js');

class AboutUsController extends BaseController {
  async getData() {
    const { ctx } = this;
    const data = await ctx.service.aboutUs.getData();
    this.result(1, data, '查询成功');
  }
}

module.exports = AboutUsController;
