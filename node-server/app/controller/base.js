'use strict';

const Controller = require('egg').Controller;

class BaseController extends Controller {
  ok(msg) {
    this.ctx.body = {
      code: 1,
      msg,
    };
  }

  fail(msg) {
    this.ctx.body = {
      code: -1,
      msg,
    };
  }

  result(code, data, msg) {
    this.ctx.body = {
      code,
      data,
      msg,
    };
  }
}

module.exports = BaseController;
