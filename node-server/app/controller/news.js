'use strict';

const BaseController = require('./base.js');

class NewsController extends BaseController {
  /**
   * 获取新闻
   * @return
   */
  async getNews() {
    const { ctx } = this;
    const obj = ctx.query;
    if (!obj.pageSize || !obj.current) {
      return this.fail('分页参数不能为空');
    }
    const data = await ctx.service.news.getNews(obj);
    const total = await ctx.service.news.getToatl(obj);
    const d = {
      data,
      total,
    };

    return this.result(1, d, '查询成功');
  }
}

module.exports = NewsController;
