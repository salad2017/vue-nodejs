'use strict';

const BaseController = require('./base.js');

class HomeController extends BaseController {
  /**
   * 获取轮播图
   * @return
   */
  async getSlideshow() {
    const { ctx } = this;
    const data = await ctx.service.home.getSlideshow();
    return this.result(1, data, '查询成功');
  }
}

module.exports = HomeController;
