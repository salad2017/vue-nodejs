'use strict';

const BaseController = require('./base.js');

class ProductController extends BaseController {
  /**
   * 获取产品数据
   * @return
   */
  async getProducts() {
    const { ctx } = this;
    const obj = ctx.query;
    if (!obj.pageSize || !obj.current) {
      return this.fail('分页参数不能为空');
    }
    const data = await ctx.service.product.getProducts(obj);
    const total = await ctx.service.product.getToatl(obj);
    const d = {
      data,
      total,
    };
    return this.result(1, d, '查询成功');
  }
}

module.exports = ProductController;
