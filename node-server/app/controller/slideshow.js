'use strict';

const BaseController = require('./base.js');

class SlideshowController extends BaseController {
  /**
   * 获取轮播图
   * @return
   */
  async getSlideshow() {
    const { ctx } = this;
    const obj = ctx.query;
    if (!obj.pageSize || !obj.current) {
      return this.fail('分页参数不能为空');
    }
    const data = await ctx.service.slideshow.getSlideshow(obj);
    return this.result(1, data, '查询成功');
  }
}

module.exports = SlideshowController;
