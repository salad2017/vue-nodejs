'use strict';

const BaseController = require('./base.js');

class AttributeController extends BaseController {
  /**
   *  根据id查询字典
   * @return
   */
  async getDataById() {
    const { ctx } = this;
    const id = ctx.query.id;
    const pageSize = ctx.query.pageSize;
    const current = ctx.query.current;
    const data = await ctx.service.attribute.getDataById(id, pageSize, current);
    return this.result(1, data, '查询成功');
  }
}

module.exports = AttributeController;
