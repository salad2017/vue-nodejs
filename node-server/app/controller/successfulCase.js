'use strict';

const BaseController = require('./base.js');

class SuccessfulCaseController extends BaseController {
  /**
   * 获取成功案例
   * @return
   */
  async getSuccessfulCase() {
    const { ctx } = this;
    const obj = ctx.query;
    if (!obj.pageSize || !obj.current) {
      return this.fail('分页参数不能为空');
    }
    const data = await ctx.service.successfulCase.getSuccessfulCase(obj);
    return this.result(1, data, '查询成功');
  }
}

module.exports = SuccessfulCaseController;
