'use strict';

const BaseController = require('./base.js');

class AskListController extends BaseController {
  /**
   *  根据id查询字典
   * @return
   */
  async add() {
    const { ctx } = this;
    const body = ctx.request.body;
    const { name, email, phone, compony, title, content } = body;
    if (!name || !email || !phone || !compony || !title || !content) {
      return this.fail('请填写完整表单再提交~');
    }
    const data = await ctx.service.askList.add(body);
    return this.result(1, data, '表单提交成功~');
  }
}

module.exports = AskListController;
