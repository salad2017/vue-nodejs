'use strict';

/**
 * 获取随机字符串
 * @param {*} l 长度
 */
exports.randomString = l => {
  const e = l || 32;
  const t = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
  const a = t.length;
  let n = '';
  for (let i = 0; i < e; i++) n += t.charAt(Math.floor(Math.random() * a));
  return n;
};
