import Vue from 'vue'
import App from './App.vue'
import router from './router/index' // 路由
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
import store from "./store";
import i18n from "./i18n";
import 'quill/dist/quill.snow.css'


Vue.config.productionTip = false


// ViewUi 多语言
Vue.use(ViewUI, {
  i18n: (key, value) => i18n.t(key, value)
})

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app')
