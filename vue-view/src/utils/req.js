import axios from "axios";
import qs from "qs";
import { Message, LoadingBar } from "view-design";

// Global axios defaults
axios.defaults.withCredentials = true;
axios.defaults.headers["X-Requested-With"] = "XMLHttpRequest";
axios.defaults.timeout = 10000;
// axios.defaults.baseURL = "https://xxx.com"

// Add a request interceptor
let loadingCount = 0;
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    if (config.headers.isLoading) {
        loadingCount++;
        LoadingBar.start()
    }
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Do something with response data
    loadingCount--;
    if (response.config.headers.isLoading) {
        if (loadingCount <= 0) {
            LoadingBar.finish();
            loadingCount = 0;
        }
    }
    return response;
}, function (error) {
    // Do something with response error
    LoadingBar.finish();
    loadingCount = 0;
    return Promise.reject(error);
});

/**
 * check status code
 * @param {*} res 
 */
function checkCode (res) {
    if (res.status === 200) {
        let hasErr = false;
        if (res.data.code == -1) {
            hasErr = true;
        }
        if (!hasErr) return;
        Message.error({
            content: res.data.msg || "系统开小差了..."
        })
    } else {
        Message.error({
            content: "错误码：" + res.status
        })
    }
}

/**
 * axios.post()
 * 
 * @param {*} url 
 * @param {*} data 
 * @param {*} headers 
 * @param {*} isLoading 
 * @param {*} isCheckError 
 */
function post (url, data = {}, headers = {}, isLoading = true, isCheckError = true, autoLogin = true) {
    if (headers && headers["Content-Type"] != "multipart/form-data") {
        data = qs.stringify(data);
    }
    return new Promise((resolve, reject) => {
        axios({
            method: "POST",
            url: url,
            data: data,
            headers: {
                "Content-Type": (headers && headers["Content-Type"]) || "application/x-www-form-urlencoded",
                "isLoading": isLoading,
                "autoLogin": autoLogin
            }
        }).then(res => {
            if (isCheckError) checkCode(res);
            resolve(res.data);
        }).catch(error => {
            // if (isCheckError) Message.error({ content:error });
            isLoading && LoadingBar.finish();
            reject(error)
        })
    })
}

/**
 *  axios.get()
 * 
 * @param {*} url
 * @param {*} data 
 * @param {*} isLoading 
 * @param {*} isCheckError
 */
function fetch (url, data = {}, isLoading = true, isCheckError = true, autoLogin = true) {
    return new Promise((resolve, reject) => {
        axios({
            method: "GET",
            url: url,
            params: data,
            headers: {
                "isLoading": isLoading,
                "autoLogin": autoLogin
            }
        }).then(res => {
            if (isCheckError) checkCode(res);
            resolve(res.data);
        }).catch(error => {
            // if (isCheckError)  Message.error({ content:error });
            reject(error)
        })
    })
}

/**
 * axios.all()
 * 
 * @param {*} arr 
 * @param {*} isLoading 
 */
function reqAll (arr = []) {
    return new Promise((resolve, reject) => {
        axios.all(arr).then(res => {
            resolve(res)
        }).catch(error => {
            reject(error)
        })
    })
}

export {
    post,
    fetch,
    reqAll
}