import { fetch, post } from "./req";

const getAboutUsApi = process.env.VUE_APP_API + "/aboutUs/getData" // 获取联系我们数据
const saveContactUsApi = process.env.VUE_APP_API + "/askList/add" // 保存联系我们
const getAttributeApi = process.env.VUE_APP_API + "/attribute/getDataById" // 数据字典查询
const getSlideshowApi = process.env.VUE_APP_API + "/slideshow/getSlideshow" // 获取轮播列表
const getProductsApi = process.env.VUE_APP_API + "/product/getProducts" // 获取产品列表
const getNewsApi = process.env.VUE_APP_API + "/news/getNews" // 获取新闻列表
const getSolutionsApi = process.env.VUE_APP_API + "/solutions/getSolutions" // 获取解决方案
const getSuccessfulCaseApi = process.env.VUE_APP_API + "/successfulCase/getSuccessfulCase" // 获取成功案例


/**
 * 获取联系我们数据
 * 
 * @param {*}  
 */
function getAboutUs() {
    return fetch(getAboutUsApi);
}

/**
 * 保存联系我们
 * 
 * @param {*}
 */
function saveContactUs(data) {
    return post(saveContactUsApi, data);
}

/**
 * 数据字典查询
 * 
 * @param {*}  {id,pageSize,current}
 */
function getAttribute(data) {
    return fetch(getAttributeApi, data);
}

/**
 * 获取轮播列表
 * 
 * @param {*}  {pageSize,current}
 */
function getSlideshow(data) {
    return fetch(getSlideshowApi, data);
}

/**
 * 获取产品列表
 * 
 * @param {*}  {pageSize,current}
 */
function getProducts(data) {
    return fetch(getProductsApi, data);
}

/**
 * 获取新闻列表
 * 
 * @param {*}  {pageSize,current}
 */
function getNews(data) {
    return fetch(getNewsApi, data);
}

/**
 * 获取解决方案
 * 
 * @param {*}  {pageSize,current}
 */
function getSolutions(data) {
    return fetch(getSolutionsApi, data);
}


/**
 * 获取成功案例
 * 
 * @param {*}  {pageSize,current}
 */
function getSuccessfulCase(data) {
    return fetch(getSuccessfulCaseApi, data);
}





export {
    getAboutUs,
    saveContactUs,
    getAttribute,
    getSlideshow,
    getProducts,
    getNews,
    getSolutions,
    getSuccessfulCase
}