import VueI18n from 'vue-i18n'
import Vue from 'vue'
import izh from 'view-design/dist/locale/zh-CN';
import ien from 'view-design/dist/locale/en-US';
import iru from 'view-design/dist/locale/ru-RU';
Vue.use(VueI18n);


export default new VueI18n({
    locale: localStorage.getItem('language') || 'zh-CN',    // 语言标识, 通过切换locale的值来实现语言切换,this.$i18n.locale 
    messages: {
        'zh-CN': Object.assign(require('./lang/zh'), izh),   // 中文语言包
        'en-US': Object.assign(require('./lang/en'), ien),    // 英文语言包
        'ru_RU': Object.assign(require('./lang/ru'), iru),    // 俄文语言包
    }
})
