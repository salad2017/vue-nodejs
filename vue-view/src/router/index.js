import Vue from "vue"
import VueRouter from "vue-router";
Vue.use(VueRouter);

import Index from '@/views/index/index.vue'
import AboutUs from '@/views/about-us/index.vue'
import Product from '@/views/product/index.vue'
import ProductDetail from '@/views/product/detail.vue'
import Solution from '@/views/solution/index.vue'
import SolutionDetail from '@/views/solution/detail.vue'
import SuccessfulCase from '@/views/successful-case/index.vue'
import SuccessfulCaseDetail from '@/views/successful-case/detail.vue'
import News from '@/views/news/index.vue'
import NewsDetail from '@/views/news/detail.vue'
import ContactUs from '@/views/contact-us/index.vue'
import Test from '@/views/test.vue'



let routes = [
    {
        path: "/",
        redirect: "/index",
    },
    {
        path: '/index', component: Index, name: 'Index',

    },
    {
        path: '*', hidden: true, redirect: { path: '/404' }
    },
    {
        path: '/about-us', component: AboutUs, name: 'AboutUs',
    },
    {
        path: '/product', component: Product, name: 'Product',
    },
    {
        path: '/product/:typeId', component: Product, name: 'Product',
    },
    {
        path: '/product/detail/:id', component: ProductDetail, name: 'ProductDetail',
    },
    {
        path: '/solution', component: Solution, name: 'Solution',
    },
    {
        path: '/solution/detail/:id', component: SolutionDetail, name: 'SolutionDetail',
    },
    {
        path: '/successful-case', component: SuccessfulCase, name: 'SuccessfulCase',
    },
    {
        path: '/successful-case/detail/:id', component: SuccessfulCaseDetail, name: 'SuccessfulCaseDetail',
    },
    {
        path: '/news', component: News, name: 'News',
    },
    {
        path: '/news/detail/:id', component: NewsDetail, name: 'NewsDetail',
    },
    {
        path: '/contact-us', component: ContactUs, name: 'ContactUs',
    }, {
        path: '/test', component: Test, name: 'Test',

    },
];

const router = new VueRouter({
    routes
})

const originalPush = VueRouter.prototype.push

VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}


export default router;
