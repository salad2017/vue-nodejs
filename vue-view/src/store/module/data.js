import {
    getSolutions, getSuccessfulCase, getAttribute, getSlideshow,
    getProducts, getNews
} from "@/utils/api.js";
import { dateFormat } from "@/utils/util";
export default {
    state: {
        solutions: [],
        successfulCase: [],
        pAttributes: [],
        slideshows: [],
        products: [],
        news: []
    },
    mutations: {
        setSolutions (state, data) {
            state.solutions = data;
        },
        setSuccessfulCase (state, data) {
            state.successfulCase = data;
        },
        setPAttributes (state, data) {
            state.pAttributes = data;
        },
        setSlideshows (state, data) {
            state.slideshows = data;
        },
        setProducts (state, data) {
            state.products = data;
        },
        setNews (state, data) {
            state.news = data;
        }
    },
    getters: {
        solutions: state => state.solutions,
        successfulCase: state => state.successfulCase,
        pAttributes: state => state.pAttributes,
        slideshows: state => state.slideshows,
        products: state => state.products,
        news: state => state.news,
    },
    actions: {
        fetchSolutions ({ commit, state }) {
            if (state.solutions.length > 0) return;
            getSolutions({
                pageSize: 100,
                current: 1,
            }).then(res => {
                commit("setSolutions", res.data || []);
            })
        },
        fetchSuccessfulCase ({ commit, state }) {
            if (state.successfulCase.length > 0) return;
            getSuccessfulCase({
                pageSize: 100,
                current: 1,
            }).then((res) => {
                commit("setSuccessfulCase", res.data || []);
            });
        },
        fetchPAttributes ({ commit, state }) {
            if (state.pAttributes.length > 0) return;
            getAttribute({
                id: "75ff73efd5e84d53ab7dfb09c84f4216",
                pageSize: 100,
                current: 1,
            }).then((res) => {
                commit("setPAttributes", res.data || []);
            });
        },
        fetchSlideshow ({ commit, state }) {
            if (state.slideshows.length > 0) return;
            getSlideshow({
                pageSize: 100,
                current: 1,
            }).then((res) => {
                commit("setSlideshows", res.data || []);
            });
        },
        fetchProducts ({ commit, state }, pageSize = 8) {
            if (state.products.length > 0) return;
            getProducts({
                pageSize: pageSize,
                current: 1,
            }).then((res) => {
                let data = res.data.data;
                data.forEach((item, index) => {
                    item.imgs = item.imgs.split(",");
                    data[index] = item;
                });
                commit("setProducts", data || []);
            });
        },
        fetchNews ({ commit, state }) {
            if (state.news.length > 0) return;
            getNews({
                pageSize: 10,
                current: 1,
            }).then((res) => {
                let data = res.data.data || [];
                data.forEach((item, index) => {
                    item.create_time = dateFormat(new Date(item.create_time), 'yyyy-MM-dd HH:mm');
                    data[index] = item;
                })
                commit("setNews", data || []);
            });
        },
    }
}