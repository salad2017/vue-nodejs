const path = require("path");
function resolve(dir) {
    return path.join(__dirname, dir)
}

const CompressionWebpackPlugin = require("compression-webpack-plugin");
const compression = new CompressionWebpackPlugin({
    filename: info => {
        return `${info.path}.gz${info.query}`
    },
    algorithm: "gzip",
    threshold: 5120,
    test: /\.(js|css)$/,
    minRatio: 0.8,
    deleteOriginalAssets: false
});

const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const uglifyJs = new UglifyJsPlugin({
    uglifyOptions: {
        warnings: false,
        compress: {
            drop_debugger: true,
            drop_console: true,
            pure_funcs: ['console.log']
        }
    },
    parallel: true
});

const useCDN = false;
const cdn = {
    css: [],
    js: [
        "https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.runtime.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/vue-router/3.1.3/vue-router.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/vuex/3.1.2/vuex.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.1/axios.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/vue-i18n/8.15.3/vue-i18n.min.js"
    ]
}


module.exports = {
    runtimeCompiler: true,
    publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
    outputDir: "dist",
    assetsDir: "assets",
    filenameHashing: true,
    lintOnSave: false,
    productionSourceMap: false,
    css: {
        loaderOptions: {
            less: {
                modifyVars: {
                    "hack": `true; @import "${resolve("src/assets/css/theme/default.less")}";`
                }
            }
        }
    },
    devServer: {
        host: "0.0.0.0",
        port: 8087,
        open: false,
        proxy: {
            "/api": {
                target: "http://127.0.0.1:7003",
                changeOrigin: true,
                pathRewrite: {
                    "^/api": "/"
                }
            }
        }
    },
    chainWebpack: config => {
        config.plugins.delete('prefetch')
        config.plugins.delete("preload");
        config.resolve.alias
            .set("@", resolve("src"))
            .set("components", resolve("src/components"))
            .set("store", resolve("src/store"))
            .set("router", resolve("src/router"))
            .set("lang", resolve("src/lang"))
            .set("utils", resolve("src/utils"))
            .set("assets", resolve("src/assets"));
        // config.module
        //     .rule("images")
        //     .use("image-webpack-loader")
        //     .loader("image-webpack-loader")
        //     .options({
        //         bypassOnDebug: true
        //     })
        //     .end();
        if (useCDN) {
            config.plugin("html")
                .tap(args => {
                    args[0].cdn = cdn;
                    return args;
                });
        }
    },
    configureWebpack: config => {
        if (useCDN) {
            config.externals = {
                "vue": "Vue",
                "vuex": "Vuex",
                "vue-router": "VueRouter",
                "axios": "axios",
                "vue-i18n": "VueI18n"
            }
        }
        if (process.env.NODE_ENV === "production") {
            config.plugins.push(compression, uglifyJs)
        }
    }
}