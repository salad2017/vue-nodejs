# vue+node.js打造官网


<div>
  <p> <a href="http://electric.txmei.cn/">点击查看DEMO</a></p align="center">
</div>



## 1. 打开前端 vue-view

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



## 2. 打开后端 node-server

<!-- add docs here for user -->

see [egg docs][egg] for more detail.

### 导入mysql数据库  ./db_base.sql

### 配置链接信息  ./config/config.default.js

### Development

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

### Deploy

```bash
$ npm start
$ npm stop
```

### npm scripts

- Use `npm run lint` to check code style.
- Use `npm test` to run unit test.
- Use `npm run autod` to auto detect dependencies upgrade, see [autod](https://www.npmjs.com/package/autod) for more detail.


[egg]: https://eggjs.org
