/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50715
 Source Host           : 127.0.0.1:3306
 Source Schema         : db_base

 Target Server Type    : MySQL
 Target Server Version : 50715
 File Encoding         : 65001

 Date: 14/10/2021 10:56:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for about_us
-- ----------------------------
DROP TABLE IF EXISTS `about_us`;
CREATE TABLE `about_us`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `content` varchar(20000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of about_us
-- ----------------------------
INSERT INTO `about_us` VALUES ('f6d4148a0bdd499c9c820f021f8a6842', '<h2 class=\"ql-align-center\"><strong style=\"color: rgb(11, 141, 215);\">Company Introduction</strong></h2><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">production and marketing of LCD Display and AIO touch kiosk.</p><p class=\"ql-align-center\">Having more than 3,000M² manufacturing square with nearly 100 employees, and registered capital of 10 million RMB, along with full series of automatic production line, including advanced manufacturing equipment and perfect testing devices, which guarantee a monthly capacity of 10000 pcs or sets LCD Digital Signage, and AIO touch kiosk.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/cd3f7410-4928-4128-a193-759aa173df27.png\"></p><p class=\"ql-align-center\">R&amp;D team possesses a crew of 15% of total staff and most of these developing engineers have more than 10 years working experience at LCD display industry. They are the base support to our new products development and customizing&nbsp;</p><p class=\"ql-align-center\">products.Under their effort, kinds of new products, such as wireless LCD Projectors,</p><p class=\"ql-align-center\">&nbsp;ultra-narrow space DigitalInformation Display (DID), etc developed from</p><p class=\"ql-align-center\">&nbsp;time to time. All product shave been approved by ISO, CCC, CE, FCC, RoHS.</p><p class=\"ql-align-center\">With perfect designed, integrated solution on both hardware and software,and superior product and service guarantee, TONME has been highly praised by domestic and overseas customers. Our LCD Display and All in one (AIO) touch kiosk have been shipped to more than 30 countries and regions all round the world, such as in the USA, Canada, Russia, Brazil, Europe, Southeast Asia, Middle</p><p class=\"ql-align-center\">East, and Africa, etc.</p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/19e4de26-e435-445b-a1db-ac0087cdbf24.png\"></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><strong style=\"color: rgb(11, 141, 215);\">Our Product</strong></p><p class=\"ql-align-center\">Digital signage, AIO touch Kiosk , Outdoor digital signage</p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/38dcb187-1581-40c4-8c81-69838ba8d630.jpg\"></p><p class=\"ql-align-center\">LCD video walls, Multi-screen LCD Video walls</p><p class=\"ql-align-center\">Freestanding totem Displays, Touch Screen Console kiosks</p><p class=\"ql-align-center\">Weatherproof LCD Totem, Harsh Environment Screens</p><p class=\"ql-align-center\">Outdoor Totem Displays, Outdoor Totems</p><p class=\"ql-align-center\">Digital Signage Solutions, Waterproof PC enclosures</p><p class=\"ql-align-center\">Touch Screen Enclosures, Sunlight Readable</p><p class=\"ql-align-center\">Drive Thru Digital Signage Menu, Industrial LCD Monitor</p><p class=\"ql-align-center\">Wireless LCD Projector， LCD interactive whiteboard</p><p class=\"ql-align-center\">4K LCD Display， New LCD Advertising Display</p><p class=\"ql-align-center\">Digital signage Media Player， HD LCD TV</p><p class=\"ql-align-center\">Wall Mounted Digital Signage， Self Service Kiosk</p><p class=\"ql-align-center\">Interactive Touch Screen Kiosk</p>', '2021-10-12 00:26:02', 'e6c835a78a134931861f07d5631a45e4', '2021-10-12 15:24:39', 'e6c835a78a134931861f07d5631a45e4');

-- ----------------------------
-- Table structure for ask_list
-- ----------------------------
DROP TABLE IF EXISTS `ask_list`;
CREATE TABLE `ask_list`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `name` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `email` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `compony` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司名称',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容',
  `product_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `status` bigint(1) NULL DEFAULT NULL COMMENT '0，未处理 1，已处理',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注，已处理状态时 备注信息',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ask_list
-- ----------------------------
INSERT INTO `ask_list` VALUES ('431fe99ff4244bf7baa1ad538aa16cc6', '测试', '166@qq.com', '156222', '测试', '测试', '123', NULL, '2021-10-13 00:12:45', 'admin', NULL, NULL, 1, NULL);
INSERT INTO `ask_list` VALUES ('9591c96c421b45a6b1b6934cae259655', '测试一全文', '19@qq.com', '119', '测试', '测试', '测试', NULL, '2021-10-12 00:27:11', 'admin', '2021-10-12 23:39:30', 'admin', 1, NULL);
INSERT INTO `ask_list` VALUES ('rM65X3KE3WMYAkMpYdDnfBmMppnkrr3r', '张三', '122131@qq.com', '10086', '测试公司', '联系我吧', '购买产品', NULL, '2021-10-12 14:34:17', '10086', NULL, NULL, 1, NULL);

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `detail` varchar(20000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情',
  `product_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '项目ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('01bcd4161d954c76a51b10d82ffcd148', '让智能生活无处不在', '<p class=\"ql-align-center\">With the development of the Internet of Things technology, more and more smart terminals equipped with commercial displays appear in cities, and new interactive methods allow people to experience a unique smart life.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">Shangxian settled in landmark buildings</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">The TONME98-inch 4K smart display terminal and ultra-narrow frame splicing screen adopted by China Zun are equipped with ADSDS hard screen technology, which makes the display screen more delicate. At the same time, the characteristics of anti-wear and anti-scratch also ensure the screen in special environments and long working hours. It can maintain a safer and more stable operation, and has irreplaceable characteristics in special display fields such as security and monitoring.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">Today, more and more high-tech buildings are inseparable from commercial displays. The passenger service information display equipment provided by TONME can be seen everywhere in the newly completed Daxing Airport, which not only provides various flight information for global passengers coming and going, but also allows staff to efficiently provide transportation services and complete production scheduling.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">TONME commercial display solutions are also fully adopted in the office building of Beijing City Sub-center and Xiong\'an Citizen Service Center. TONME patchwork has only a 0.99mm ultra-narrow bezel LCD screen and 74 screens to form a heart-shaped splicing screen, which vividly and clearly displays all kinds of government information, adding a new modern digital landmark to Xiongan.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">5G brings more opportunities to commercial display</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">In smart cities, commercial displays have become a landscape among urban buildings. Especially with the early arrival of 5G commercialization and the rapid development of the Internet of Things, artificial intelligence, and big data, \"information video and ultra-high-definition video\" have become a major trend in the development of the global information industry, and display screens are becoming people’s understanding of the city , An important port for managing the city and enjoying city life.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">According to analysis, in 2018, the sales of commercial display terminals such as educational whiteboards, splicing screens, and advertising machines in China have reached 41.9 billion yuan, and in 2022, it may grow into a huge market of 100 billion yuan and bring more markets. opportunity.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">The 5G era is an era of rapid development of information, opening a door to the construction of smart cities. With the acceleration of 5G, lifestyles such as smart transportation, smart medical care, distance education, and smart security will get closer and closer to our lives.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">TONME is deeply integrating display, big data, artificial intelligence and other technologies to bring people Internet of Things solutions in more segments.</p><p class=\"ql-align-center\">Ubiquitous commercial displays dot the bustling city, allowing more people to experience the wisdom and convenience brought by the Internet of Things, and adorn people\'s colorful lives.</p>', NULL, '2021-10-12 14:54:52', 'admin', '2021-10-12 15:03:18', 'admin');
INSERT INTO `news` VALUES ('e9293daa61034445a5cc54747fe61c8f', 'Kiosk是什么', '<p class=\"ql-align-center\">A kiosk (pronounced KEE-ahsk) is a small, free-standing physical structure that displays information or provides a service. Kiosks can be manned or unmanned, and unmanned kiosks can be digital or non-digital. The word kiosk is of French, Turkish and Persian origin and means pavilion or portico.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">In business, kiosks are often used in locations with high foot traffic. In a shopping mall, for example, an unmanned, non-digital kiosk can be placed near entrances to provide people passing by with directions or promotional messaging. Manned kiosks temporarily set up in aisles can provide businesses that have seasonal sales cycles with a cost-effective way to display wares, and digital kiosks placed near movie theaters can provide online banking or ticket sales services.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">Unmanned digital kiosks that provide customers with self-service capabilities are increasingly being used for such things as hotel check-in, retail sales check-out and healthcare screenings in pharmacies for vital information such as blood pressure. Amazon and Walmart are currently experimenting with how to optimize the click-and-mortar experience, testing kiosks that dispense merchandise previously ordered online.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">When an unmanned kiosk is programmed with software that incorporates artificial intelligence, it can provide customers with an experience that, in some cases, is quite similar to that of a manned kiosk. For example, an intelligent check-in kiosk at an airport can monitor a variety of data sources, including passenger check-in flow, and programmatically request additional kiosks to be activated in real time during busy periods.</p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/593c45df-51c6-45d9-ace5-27364cf8e314.jpg\"></p>', NULL, '2021-10-12 14:56:02', 'admin', '2021-10-12 14:56:32', 'admin');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `type_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型ID',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '产品名称',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `imgs` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片 逗号隔开',
  `detail` varchar(20000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('2eb4d5bf326b4a0990061f8b34e1cfdd', '385ee039f05a4a8f97fac47ba0cd4ae6', '面部识别', '', 'http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/0531e20a-1922-4ca6-83e2-82c0a400f2e5.jpg,http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/2ebb6d61-f1d2-4408-8c0d-b07ad57033fe.jpg,http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/4b2b692b-7045-4e8b-82d4-09fdcbf3eed2.jpg,http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/90d1cee7-af23-459c-99a0-310e740dcd90.jpg', '<p><span style=\"color: rgb(34, 34, 34);\">Hot Tags: factory price forehead body ai facial recognition with temperature kiosk, China, suppliers, factory, wholesale, customized, price, cheap, bulk,&nbsp;</span><a href=\"https://www.tmdigitalsignage.com/showroom/55%E2%80%9D-Outdoor-Freestanding-Digital-Poster/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(34, 34, 34);\">55” Outdoor Freestanding Digital Poster</a><span style=\"color: rgb(34, 34, 34);\">,&nbsp;</span><a href=\"https://www.tmdigitalsignage.com/showroom/Freestanding-Digital-Signage/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(34, 34, 34);\">Freestanding Digital Signage</a><span style=\"color: rgb(34, 34, 34);\">,&nbsp;</span><a href=\"https://www.tmdigitalsignage.com/showroom/Smart-LCD-Touch-Table/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(34, 34, 34);\">Smart LCD Touch Table</a><span style=\"color: rgb(34, 34, 34);\">,&nbsp;</span><a href=\"https://www.tmdigitalsignage.com/showroom/LCD-Advertising-Display/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(34, 34, 34);\">LCD Advertising Display</a><span style=\"color: rgb(34, 34, 34);\">,&nbsp;</span><a href=\"https://www.tmdigitalsignage.com/showroom/22%E2%80%9D-Vehicle-Mount-Display/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(34, 34, 34);\">22” Vehicle Mount Display</a><span style=\"color: rgb(34, 34, 34);\">,&nbsp;</span><a href=\"https://www.tmdigitalsignage.com/showroom/Floor-Standing-Kiosk/\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(34, 34, 34);\">Floor-Standing Kiosk</a></p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/9aa9d117-c7d6-4c27-ac68-985685fe58b9.jpg\"></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/7ccecc83-e21c-412c-9e17-8a7e8526184c.jpg\"></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/b6711648-8a94-46dd-b32e-389797d9b467.jpg\"></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/72e52b89-c42d-45d4-8701-33f791f2dbb7.jpg\"></p>', '2021-10-12 15:16:08', 'admin', '2021-10-12 15:18:22', 'admin');

-- ----------------------------
-- Table structure for slideshow
-- ----------------------------
DROP TABLE IF EXISTS `slideshow`;
CREATE TABLE `slideshow`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `img_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片地址',
  `url` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转地址',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of slideshow
-- ----------------------------
INSERT INTO `slideshow` VALUES ('11ae50fdd78a4833ae6905f079035f1b', 'http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/dabf1a06-4729-45d4-af47-c49752327776.jpg', 'http://electric.txmei.cn/#/product/detail/be170aa8d8d6401abf448b45f1424294', '2021-10-12 14:47:11', 'admin', NULL, NULL);
INSERT INTO `slideshow` VALUES ('83facc66b9004c3696bfa43a58c1cb61', 'http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/735daac7-c048-4841-81d2-eb770a921e99.jpg', 'https://www.baidu.com', '2021-10-12 14:45:36', 'admin', '2021-10-12 20:16:14', 'admin');
INSERT INTO `slideshow` VALUES ('e49022197e86465fbd979b8e17f905de', 'http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/53c8918d-717e-4641-a5d5-974c2114f55f.jpg', 'http://electric.txmei.cn/#/product/detail/be170aa8d8d6401abf448b45f1424294', '2021-10-12 14:47:21', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for solution
-- ----------------------------
DROP TABLE IF EXISTS `solution`;
CREATE TABLE `solution`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '方案名称\r\n方案名称\r\n方案名称',
  `img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `detail` varchar(20000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of solution
-- ----------------------------
INSERT INTO `solution` VALUES ('42b0c9ed7a7b409ea25d269b9c949db9', '信息发布', 'http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/68025a79-1523-401d-b7da-789ad9f1a2c2.jpg', '<p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/0b966d0e-b1b8-40ce-b4f2-08038a0741aa.jpg\"></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">Multimedia can often be thought of as a new technology or product that combines advanced computer technology with video, audio and communications technologies. Therefore, we believe that multimedia is a computer integrated processing of multimedia information such as text, graphics, images, audio, video, etc., so that a variety of information to establish a logical connection, integrated into a system and interactive. It is a rapidly developing comprehensive electronic information technology that has brought profound revolution to people\'s work, life, and entertainment.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">When it comes to multimedia information systems, we have to mention computer information systems. Computer information system refers to the system that provides information and assists people in controlling and making decisions on the environment. It is a modernized tool and means based on computers and communication networks, and serves the information processing system in the field of management. It is composed of computer science and information science. A discipline that has been developed through the penetration of multiple disciplines such as management science. Multimedia information system is the advanced development direction of multimedia information of computer information system. It is a new generation of highly integrated, powerful and intelligent information system. The multimedia information system is not limited to text and numerical values, but also can use a large amount of information of various media such as images, dynamic videos, and sounds, and has better information performance, better interactivity, and greater use of information.</p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">Tonme Digital Multimedia Information Publishing System is a powerful information publishing software independently developed by the company. It is specially used for editing and publishing of various display terminals such as PDP, DLP, LCD, video wall system, LED, etc. The system uses two kinds of systems. Compatible structural mode (C/S, B/S), which can realize cross-platform management of multiple operating system terminals, and select functional modules according to the actual needs of users, providing diversified consulting release management functions.</p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/db3cf94d-e8cb-4050-97c5-029451bb513c.png\"></p>', '2021-10-12 14:40:46', 'admin', '2021-10-12 14:44:18', 'admin');
INSERT INTO `solution` VALUES ('56c769c688de402680f13f9cf693c5f9', '智能卡片', 'http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/56a0439b-34e3-4d7a-a869-e7aefe5f3d1c.jpg', '<p><br></p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/774328e3-1b75-407c-a516-9a26bbad9794.png\"></p><p><span style=\"color: rgb(255, 255, 255); background-color: rgb(153, 51, 255);\">The Smart Class Card is a multi-functional interactive terminal that can combine the teacher\'s subject teaching, the school\'s moral education work, and the school\'s various publicity messages and campus cards into various fields, so that students and teachers can look and apply. The smart class card can handle the problem of “walking class” attendance management, and combines emerging technologies such as cloud computing, Internet of Things and virtual reality technology with class management to enhance school management and management effectiveness.</span></p><p><br></p><p><br></p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/8a004026-8149-494f-aff5-d6d0521fcec3.png\"></p><p><br></p><p><br></p><p><span style=\"color: rgb(51, 51, 51); background-color: rgb(0, 102, 204);\">The attendance function can also be connected with the school environment detection detector to instantly display the school\'s environmental inspection data, and summarize the data into the background for multi-dimensional statistical analysis, so that the smart class card becomes the school to realize the Internet of Things, big data. The entrance to information collection also provides key data support for the day-to-day management of the school.</span></p>', '2021-10-12 14:43:39', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for successful_case
-- ----------------------------
DROP TABLE IF EXISTS `successful_case`;
CREATE TABLE `successful_case`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '案例名称',
  `img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `detail` varchar(20000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of successful_case
-- ----------------------------
INSERT INTO `successful_case` VALUES ('3bd4231feddf43c3a9a74af9c9d19934', '公交车站的广告牌', 'http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/23e06e81-0514-4299-92bd-0ec028082a41.jpg', '<p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/d5fed533-6a8c-448a-bff2-25a160f04c74.jpg\"></p><p class=\"ql-align-center\"><br></p><p>Public bus station billboard generally refers to the construction of a rectangular light box in the bus shelter, adding a picture or video on one side or both sides of the light box for advertising, also known as the bus station light box advertising. The advertisement of bus station billboard standing in the big people flow, making people feel a kind of visual pleasure while waiting, the advertising effect is unique. The interaction with the bus body advertising highlights the comprehensive advantages of public transport advertising.</p><p><br></p><p>Compared with other outdoor media, the bus station billboard can deliver more specific information content than the large outdoor advertisement due to the repeated and relatively long stay of the waiting people. It not only passes brand information, but also tell some details about the promotion.</p><p><br></p><p>Bus station billboard can help customers reach the largest target audience in the shortest time. It can quickly and effectively improve the visibility of brands, which itself is a landscape of beautifying the urban environment. The brand information transmitted by the bus station billboard advertisement is concise and direct, does not occupy the consumer\'s time, and is not easily transferred like a TV broadcast advertisement. It does not turn pages like a newspaper, which can effectively avoid the loss of media delivery costs. Also night lighting system makes the picture better visual impact.</p><p><br></p><p>Tonme cooperated with Shenzhen Public Bus and built over 500 pcs billboards in different bus stations across Shenzhen. Different advertisements on Tonme bus station billboard are playing every single day, reaching its customers on their fine way.</p>', '2021-10-12 12:40:39', 'admin', NULL, NULL);
INSERT INTO `successful_case` VALUES ('450b271570164936980a98dfc4de4aa7', '时装店数码显示', 'http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/e692934b-07b1-4a32-babf-536a9f6e9d0e.jpg', '<p>With the rapid development of social economy and culture, fashion has gradually become a hot topic, and the development of the apparel industry in recent years is very impressive. How to change the traditional propaganda method, use digital signage with more informational, advanced and fashionable method to advertise is so important. Customers can know the product information through videos, pictures, texts in digital display.</p><p><br></p><p>Tonme supplied ANGELO chain store 300 pcs floor standing digital signage for indoor advertising. The 65” display with high-definition images, 1920*1080 resolution, and its delicate design perfectly suites the fashion store decoration. Also Tonme helps install ANGELO’s own APK in Android system.</p><p><br></p><p>Our advertising display not only plays product video and image anytime anywhere, but also can publish text information (notifications, announcements, news, weather forecasts, holiday greetings, slogans and other rolling subtitles).</p><p><br></p><p class=\"ql-align-center\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/ab655098-a9de-4b4f-9a56-e9af243ba74c.jpg\"></p><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\"><span style=\"color: rgb(51, 51, 51);\">Tonme digital signage saves ANGELO much labor promotion costs, and its fashionable, novel propaganda method add a lot of popularity to the store.</span></p>', '2021-10-12 12:44:16', 'admin', NULL, NULL);
INSERT INTO `successful_case` VALUES ('4549f9963c9640cb863992d62d3d9bef', '机场数字显示', 'http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/df5a3b6d-e9de-45c5-9f8a-e3a441faf821.jpg', '<p><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/ee0f4ed8-2ca7-4ef5-9ad3-22fee884316c.jpg\"></p><p><br></p><p><span style=\"color: rgb(51, 51, 51);\">The airport information release system is assembled by a large-screen display system, which can be displayed on a single screen through a network multi-screen processor or displayed in any size across the screen, moving and zooming. The display mode setting function include fixed display mode and cyclic display mode, each display task can be displayed on the screen fixedly.</span></p><p><br></p><p><span style=\"color: rgb(51, 51, 51);\">Also, multiple display tasks can be edited into a group in a certain order, and each task can be displayed in turn according to the set sequence and time interval. The large-screen display system also is able to edit and save multiple cyclically displayed task groups, and display different task groups cyclically based on different needs.</span></p><p><br></p><p><span style=\"color: rgb(51, 51, 51);\">During the design of the airport large-screen display system, real-time video signal display is required. The airport large-screen display system developed by Tonme supports NTSC, PAL or SECAM full-standard video signal input, and also supports direct SDI format digital video display.</span></p><p><br></p><p><span style=\"color: rgb(51, 51, 51);\">Tonme large-screen display system supports display by networking multi-screen processor and built-in image processing module. These video signals can be roamed and scaled at any size and position on the combined screen, and can be superimposed with the input RGB signal and network signal to ensure real-time performance for all video signals.</span></p><p><br></p><p><span style=\"color: rgb(51, 51, 51);\">The application of large-screen display system in airport is to provide tourists with more convenient, more informational communication with improved efficiency, and more timely travel experience. It is more necessary to intelligently share information in real time, especially in the waiting area, aisle corridor, airport hall, security checkpoint and other areas where traffic is concentrated.</span></p><p><br></p><p><span style=\"color: rgb(51, 51, 51);\">Tonme successfully designed the large-screen multimedia information release system for Guilin Airport, including interactive AIO, wall-mounted digital signage, multimedia player controller and streaming media encoder.</span></p>', '2021-10-12 12:22:56', 'admin', '2021-10-12 12:26:29', 'admin');
INSERT INTO `successful_case` VALUES ('6494524d70fe40a0b6e51dfbea712ac8', '银行自助服务亭', 'http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/6d0d5e1e-84f0-431b-a681-8b0fc6ac3098.jpg', '<p>The application of self-service terminals has become more and more extensive, and its comprehensive features and functions have led to a large demand for in various industries. At present, the needs for self-service terminals in various fields has shown a steady growth trend, while bank self-service kiosks occupy a large share in the market. In the future, as the pace of life continues to accelerate, the technology of self-service terminal gets more and more advanced, especially the application of new technologies such as artificial intelligence is deepening, the application of self-service terminals in various industries will be further improved, and the design will be more user-friendly and function more diverse.</p><p><br></p><p>Tonme cooperated with Bank of China, Shantou Branch, supplied over 100 pcs self-service kiosks. All these self-service kiosks are sprayed with high-quality outdoor plastic powder, which is rust-proof and wear-resistant. The mainboards are from well-known domestic manufacturers, functioning well and stable. The self-service terminal adopts streamlined design and operation is user-friendly, simple and convenient.</p><p class=\"ql-align-justify ql-indent-2\"><img src=\"http://bt.txmei.cn:8888/down/Twpj5COmhBA8?fname=/vue_shiro_photo/userImg/5bd033c4-1028-4800-b61d-45692c1fbcd2.jpg\"></p><p><br></p><p>For a very special place like bank, it is very necessary to install a self-service terminal because of large number of people flowing in and too much daily business to deal with. Bank self-service kiosk helps bank staff better manage the bank business, its 24 hours all-around working also better serve customers.</p>', '2021-10-12 12:27:50', 'admin', '2021-10-12 12:39:09', 'admin');

-- ----------------------------
-- Table structure for sys_attribute
-- ----------------------------
DROP TABLE IF EXISTS `sys_attribute`;
CREATE TABLE `sys_attribute`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_attribute
-- ----------------------------
INSERT INTO `sys_attribute` VALUES ('75ff73efd5e84d53ab7dfb09c84f4216', '产品类型');

-- ----------------------------
-- Table structure for sys_attribute_detail
-- ----------------------------
DROP TABLE IF EXISTS `sys_attribute_detail`;
CREATE TABLE `sys_attribute_detail`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `attrId` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '属性id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性明细id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_attribute_detail
-- ----------------------------
INSERT INTO `sys_attribute_detail` VALUES ('385ee039f05a4a8f97fac47ba0cd4ae6', '75ff73efd5e84d53ab7dfb09c84f4216', '显示器');
INSERT INTO `sys_attribute_detail` VALUES ('8b0be3af6d3d46e7aa90cf4073931b13', 'd73d7a9c471f4aba884e79152cb1ace0', '模块');
INSERT INTO `sys_attribute_detail` VALUES ('cba408efa74a41bc9c9c3a3db4e9c12e', '75ff73efd5e84d53ab7dfb09c84f4216', '智能电视');

SET FOREIGN_KEY_CHECKS = 1;
